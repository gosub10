<?xml version="1.0" encoding="UTF-8"?>
<!--
GOSUB10.xsl == Transform master XML to page XHTML.
Copyright (C) 2008, 2009  Claude Heiland-Allen <claudiusmaximus@goto10.org>
--> 
<xsl:stylesheet version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:str="http://exslt.org/strings"
 xmlns="http://www.w3.org/1999/xhtml">
<xsl:output method="xml" indent="yes" encoding="UTF-8"
 doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
 doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd" />

<!-- what category of page are we to render -->
<xsl:param name="what"/>

<!-- which item within that category are we to render -->
<xsl:param name="which"/>

<!-- one main template -->
<xsl:template match="/">
<html xml:lang="en" lang="en">

  <head>
    <link rel="stylesheet" type="text/css" href="GOSUB10.css" />
    <link rel="shortcut icon" type="image/png" href="icon.png" />
    <link rel="alternate" type="application/rss+xml" href="releases.rss" title="GOSUB10 releases RSS feed" />
    <title>
      GOSUB10 ::
      <xsl:choose>
        <xsl:when test="$what='index'">Index</xsl:when>
        <xsl:when test="$what='releases'">Releases</xsl:when>
        <xsl:when test="$what='artists'">Artists</xsl:when>
        <xsl:when test="$what='mission'">Mission</xsl:when>
        <xsl:when test="$what='contact'">Contact</xsl:when>
        <xsl:when test="$what='radio'">Radio</xsl:when>
        <xsl:when test="$what='legal'">Legal</xsl:when>
        <xsl:when test="$what='artist'">Artists ::
          <xsl:value-of select="label/artists/artist[artistid=$which]/artistname" />
        </xsl:when>
        <xsl:when test="$what='release'">Releases ::
          [<xsl:value-of select="$which" />]
          <xsl:choose>
            <xsl:when test="label/releases/release[releaseid=$which]/variousartists='yes'">
              Various Artists
            </xsl:when>
            <xsl:otherwise>
              <xsl:value-of select="label/releases/release[releaseid=$which]/tracks/track[1]/tags/artist" />
            </xsl:otherwise>
          </xsl:choose>
          -
          <xsl:value-of select="label/releases/release[releaseid=$which]/tracks/track[1]/tags/album" />
        </xsl:when>
      </xsl:choose>
    </title>
  </head>

  <body>
    <div id="top">
      <div id="topleft"></div>
      <div id="topcenter"><h1><a href="./" title="GOSUB10"><img src="GOSUB10.gif" alt="GOSUB10" /></a></h1></div>
    </div>
    <div id="mid">
      <div id="midleft">
        <ul>
          <li><a class="menu" href="releases.html">releases</a></li>
          <li><a class="menu" href="artists.html">artists</a></li>
          <li><a class="menu" href="radio.html">radio</a></li>
          <li><a class="menu" href="mission.html">mission</a></li>
          <li><a class="menu" href="contact.html">contact</a></li>
          <li><a class="menu" href="legal.html">legal</a></li>
        </ul>
      </div>
      <div id="midright">

        <xsl:choose>
        <xsl:when test="$what='index'">
          <h2>Welcome</h2>
          <div class="contact">
            <xsl:copy-of select="label/welcome/node()" />
          </div>
        </xsl:when>

        <xsl:when test="$what='radio'">
          <h2>GOSUB10 Radio</h2>
          <div class="radio">
            <xsl:copy-of select="label/radio/node()" />
          </div>
        </xsl:when>

        <xsl:when test="$what='legal'">
          <h2>Legal Information</h2>
          <div class="legal">
            <xsl:copy-of select="label/legal/node()" />
          </div>
        </xsl:when>

        <xsl:when test="$what='releases'">
          <h2>Releases</h2>
          <ul>
            <xsl:for-each select="label/releases/release">
              <li>
                <xsl:choose>
                <xsl:when test="variousartists='yes'">
                  <a href="{releaseid}.html" title="Various Artists - {tracks/track[1]/tags/album}">
                    Various Artists
                    -
                    <xsl:value-of select="tracks/track[1]/tags/album" />
                    [<xsl:value-of select="releaseid" />]
                  </a>
                </xsl:when>
                <xsl:otherwise>
                  <a href="{releaseid}.html" title="{tracks/track[1]/tags/artist} - {tracks/track[1]/tags/album}">
                    <xsl:value-of select="tracks/track[1]/tags/artist" />
                    -
                    <xsl:value-of select="tracks/track[1]/tags/album" />
                    [<xsl:value-of select="releaseid" />]
                  </a>
                </xsl:otherwise>
                </xsl:choose>
              </li>
            </xsl:for-each>
          </ul>
        </xsl:when>

        <xsl:when test="$what='artists'">
          <h2>Artists</h2>
          <ul>
            <xsl:for-each select="label/artists/artist">
              <li>
                <a href="{artistid}.html" title="{artistname}">
                <xsl:value-of select="artistname" />
                </a>
              </li>
            </xsl:for-each>
          </ul>
        </xsl:when>

        <xsl:when test="$what='release'">
        <div class="release">
          <h2>
            <xsl:choose>
              <xsl:when test="label/releases/release[releaseid=$which]/variousartists='yes'">
                Various Artists
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="label/releases/release[releaseid=$which]/tracks/track[1]/tags/artist" />
              </xsl:otherwise>
            </xsl:choose>
            -
            <xsl:value-of select="label/releases/release[releaseid=$which]/tracks/track[1]/tags/album" />
            [<xsl:value-of select="$which" />]
          </h2>
          <div class="cover">
            <img src="{$which}_400x400.png" alt="cover artwork" />
          </div>
          <div class="tracks">
            <table>
              <xsl:for-each select="label/releases/release[releaseid=$which]/tracks/track">
                <tr>
                  <td><a href="http://www.archive.org/download/{../../releaseid}/{filename}" title="download (FLAC)"><img src="download.png" alt="download (FLAC)" /></a></td>
                  <td><a href="http://www.archive.org/download/{../../releaseid}/{str:replace(filename,'.flac','.ogg')}" title="download (Ogg Vorbis)"><img src="download.png" alt="download (Ogg Vorbis)" /></a></td>
                  <!--<td><a href="releases/{../../releaseid}/{str:replace(filename,'.flac','.m3u')}" title="play"><img src="play.png" alt="play" /></a></td>--><!-- FIXME when playback works -->
                  <td>
                    <xsl:choose>
                      <xsl:when test="../../variousartists='yes'">
                        <xsl:for-each select="tags/artist">
                          <xsl:variable name="artistname" select="." />
                          <xsl:if test="position() != 1">, </xsl:if>
                          <a href="{/label/artists/artist[artistname=$artistname]/artistid}.html" title="{$artistname}"><xsl:value-of select="$artistname" /></a>
                        </xsl:for-each> - 
                        <xsl:value-of select="tags/title" />
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:value-of select="tags/title" />
                      </xsl:otherwise>
                    </xsl:choose>
                  </td>
                </tr>
              </xsl:for-each>
              <tr class="all">
                <td>&#160;</td>
                <td><a href="releases/{$which}.tgz" title="download all (Ogg Vorbis)"><img src="download.png" alt="download all (Ogg Vorbis)" /></a></td>
                <!--<td><a href="releases/{$which}.m3u" title="play all"><img src="play.png" alt="play all" /></a></td>--><!-- FIXME when playback works -->
                <td>download the whole release</td><!-- FIXME when playback works -->
              </tr>
              <tr class="archive">
                <td colspan="3">
                  <a href="http://www.archive.org/details/{$which}" title="{$which} on the Internet Archive">
                    <xsl:value-of select="$which" /> on the Internet Archive
                  </a>
                </td>
              </tr>
            </table>
          </div>
          <div class="description">
            <xsl:copy-of select="label/releases/release[releaseid=$which]/description/node()" />
            <p>
              <xsl:value-of select="$which" /> was released on
              <xsl:value-of select="label/releases/release[releaseid=$which]/releasedate/human" />.
            </p>
          </div>
        </div>
        </xsl:when>

        <xsl:when test="$what='artist'">
        <div class="artist">
          <h2><xsl:value-of select="label/artists/artist[artistid=$which]/artistname" /></h2>
          <div class="cover">
            <img src="{$which}_400x400.png" alt="photo" />
          </div>
          <div class="description">
            <xsl:copy-of select="label/artists/artist[artistid=$which]/description/node()" />
          </div>
          <div class="releases">
            <h3>Releases featuring <xsl:value-of select="label/artists/artist[artistid=$which]/artistname" /></h3>
            <ul>
              <xsl:for-each select="label/releases/release[tracks/track/tags/artist=/label/artists/artist[artistid=$which]/artistname]">
              <li>
                <a href="{releaseid}.html" title="{tracks/track[1]/tags/album}">
                <xsl:choose>
                  <xsl:when test="variousartists='yes'">
                    Various Artists
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="tracks/track[1]/tags/artist" />
                  </xsl:otherwise>
                  </xsl:choose>
                  -
                  <xsl:value-of select="tracks/track[1]/tags/album" />
                  [<xsl:value-of select="releaseid" />]
                </a>
              </li>
              </xsl:for-each>
            </ul>
          </div>
        </div>
        </xsl:when>

        <xsl:when test="$what='mission'">
          <h2>Mission</h2>
          <div class="mission">
            <xsl:copy-of select="label/mission/node()" />
          </div>
        </xsl:when>

        <xsl:when test="$what='contact'">
          <h2>Contact</h2>
          <div class="contact">
            <xsl:copy-of select="label/contact/node()" />
          </div>
        </xsl:when>

        </xsl:choose>

        <div class="clear">&#160;</div>
      </div>
    </div>
    <div id="bot">
      <div id="botleft">&#160;</div>
      <div id="botright"><a href="http://goto10.org" title="GOTO10">GOTO10</a></div>
    </div>
  </body>

</html>
</xsl:template>

</xsl:stylesheet>
