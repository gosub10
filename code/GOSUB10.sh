#!/bin/bash
#
# GOSUB10.sh -- From source releases to output site.
# Copyright (C) 2008, 2009, 2011  Claude Heiland-Allen <claudiusmaximus@goto10.org>
#
# USAGE:  ./GOSUB10.sh ./config
#

if [[ "x${1}" == "x" ]]
then
  cat <<-USAGE
Usage:
    $(basename "${0}") [config] -- generate output site from "config"
USAGE
  exit 1
fi

#=======================================================================
echo 'Reading configuration...'
#=======================================================================

CONFIG="$1"
source "$CONFIG"

#=======================================================================
echo 'Beginning to prepare master XML...'
#=======================================================================

echo "<label>" > "$XML"
echo "<labelname>$LABEL</labelname>" >> "$XML"

#=======================================================================
echo 'Extracting and combining label metadata...'
#=======================================================================

cat "$LABELDIR/"*.xml >> "$XML"

#=======================================================================
echo 'Extracting and combining artist metadata...'
#=======================================================================
(
  echo "<artists>"
  find "$ARTISTDIR" -maxdepth 1 -mindepth 1 -type d -not -name ".svn" |
  sort |
  while read A ;
  do
    ARTIST=$(basename "$A")
    ARTISTXML=$(echo "$ARTIST" | sed 's-&-&amp;-g' | sed 's-<-&lt;-g' | sed 's->-&gt;-g')
    echo "<artist>"
    echo "<artistid>$ARTISTXML</artistid>"
    cat "$A/meta.xml"
    echo "</artist>"
  done
  echo "</artists>"
) >> "$XML"

#=======================================================================
echo 'Extracting and combining release metadata...'
#=======================================================================
(
  echo "<releases>"
  find "$RELEASEDIR" -maxdepth 1 -mindepth 1 -type d -not -name ".svn" |
  sort |
  while read R ;
  do
    RELEASE=$(basename "$R")
    RELEASEXML=$(echo "$RELEASE" | sed 's-&-&amp;-g' | sed 's-<-&lt;-g' | sed 's->-&gt;-g')
    echo "<release>"
    echo "<releaseid>$RELEASEXML</releaseid>"
    cat "$R/meta.xml"
    echo "</release>"
  done
  echo "</releases>"
) >> "$XML"

#=======================================================================
echo 'Finalizing master XML...'
#=======================================================================

echo "</label>" >> "$XML"

#=======================================================================
echo 'Generating XHTML pages...'
#=======================================================================

xsltproc releaseids.xsl "$XML" |
while read RELEASE ;
do
  if [[ "$RELEASE" != "" ]] ;
  then
    xsltproc --stringparam what "release" --stringparam which "$RELEASE" \
      GOSUB10.xsl "$XML"  >"$OUTPUT/$RELEASE.html"
  fi
done
xsltproc artistids.xsl "$XML" |
while read ARTIST ;
do
  if [[ "$ARTIST" != "" ]] ;
  then
    xsltproc --stringparam what "artist" --stringparam which "$ARTIST" \
      GOSUB10.xsl "$XML"  >"$OUTPUT/$ARTIST.html"
  fi
done
xsltproc --stringparam what "index" \
  GOSUB10.xsl "$XML" > "$OUTPUT/index.html"
xsltproc --stringparam what "radio" \
  GOSUB10.xsl "$XML" > "$OUTPUT/radio.html"
xsltproc --stringparam what "legal" \
  GOSUB10.xsl "$XML" > "$OUTPUT/legal.html"
xsltproc --stringparam what "releases" \
  GOSUB10.xsl "$XML" > "$OUTPUT/releases.html"
xsltproc --stringparam what "artists" \
  GOSUB10.xsl "$XML" > "$OUTPUT/artists.html"
xsltproc --stringparam what "mission" \
  GOSUB10.xsl "$XML" > "$OUTPUT/mission.html"
xsltproc --stringparam what "contact" \
  GOSUB10.xsl "$XML" > "$OUTPUT/contact.html"

#=======================================================================
echo 'Generating RSS feeds...'
#=======================================================================

xsltproc GOSUB10_rss.xsl "$XML" > "$OUTPUT/releases.rss"

#=======================================================================
echo 'Copying static assets...'
#=======================================================================

cp "$STATICDIR/"* "$OUTPUT/"
cp "$ARTISTDIR/"*"/"*"_400x400.png" "$OUTPUT/"
cp "$RELEASEDIR/"*"/"*"_400x400.png" "$OUTPUT/"

#=======================================================================
echo 'Done!'
#=======================================================================
