#!/bin/bash
#
# flac2xml.sh -- Generate an XML file from FLAC metadata
# Copyright (C) 2008  Claude Heiland-Allen <claudiusmaximus@goto10.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# USAGE:  ./flac2xml.sh file.flac > file.xml
#
oldIFS="$IFS"
export IFS="="
FLACFILE="$1"
if [[ "empty" == "empty$FLACFILE" ]] ; then exit ; fi
FLACFILEXML=$(echo "$FLACFILE" | sed 's-&-&amp;-g' | sed 's-<-&lt;-g' | sed 's->-&gt;-g')
FLACFILEBASE=$(basename "$FLACFILE")
FLACFILEBASEXML=$(echo "$FLACFILEBASE" | sed 's-&-&amp;-g' | sed 's-<-&lt;-g' | sed 's->-&gt;-g')
SAMPLERATE=$(metaflac --show-sample-rate "$FLACFILE")
SAMPLECOUNT=$(metaflac --show-total-samples "$FLACFILE")
RUNTIMEMINS=$(echo "$SAMPLECOUNT/$SAMPLERATE/60" | bc)
RUNTIMESECS=$(echo "$SAMPLECOUNT/$SAMPLERATE-60*$RUNTIMEMINS" | bc)
RUNTIMETMS=$(echo "1000*$SAMPLECOUNT/$SAMPLERATE" | bc)
echo "<track>"
echo "<filename>$FLACFILEBASEXML</filename>"
echo "<runtime><minutes>$RUNTIMEMINS</minutes><seconds>$RUNTIMESECS</seconds><totalms>$RUNTIMETMS</totalms></runtime>"
echo "<tags>"
metaflac --no-utf8-convert --export-tags-to=- "$FLACFILE" |
while read K V ;
do
  KEY=$(echo "$K" | tr [:upper:] [:lower:])
  VALUE=$(echo "$V" | sed 's-&-&amp;-g' | sed 's-<-&lt;-g' | sed 's->-&gt;-g')
  echo "<$KEY>$VALUE</$KEY>"
done
echo "</tags>"
echo "</track>"
IFS="$oldIFS"
