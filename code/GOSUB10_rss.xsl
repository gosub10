<?xml version="1.0" encoding="UTF-8"?>
<!--
GOSUB10_rss.xsl == Transform master XML to RSS feed.
Copyright (C) 2008, 2009  Claude Heiland-Allen <claudiusmaximus@goto10.org>
-->
<xsl:stylesheet
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  version="1.0">
<xsl:output method="xml" indent="yes" encoding="UTF-8" />
<xsl:template match="/">
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>
<atom:link href="http://gosub10.org/releases.rss" rel="self" type="application/rss+xml" />
<title>GOSUB10 :: Releases</title>
<link>http://gosub10.org</link>
<description>Recent releases from the GOSUB10 netlabel.</description>
<xsl:for-each select="label/releases/release">
  <item>
  <title>
    <xsl:choose>
      <xsl:when test="variousartists='yes'">
        Various Artists - <xsl:value-of select="tracks/track[1]/tags/album" />
      </xsl:when>
    <xsl:otherwise>
      <xsl:value-of select="tracks/track[1]/tags/artist" /> - <xsl:value-of select="tracks/track[1]/tags/album" />
    </xsl:otherwise>
    </xsl:choose>
  </title>
  <link>http://gosub10.org/<xsl:value-of select="releaseid" />.html</link>
  <guid>http://gosub10.org/<xsl:value-of select="releaseid" />.html</guid>
  <description><xsl:text disable-output-escaping="yes">&lt;![CDATA[</xsl:text><xsl:copy-of select="description/node()" /><xsl:text disable-output-escaping="yes">]]&gt;</xsl:text></description>
  <pubDate><xsl:value-of select="releasedate/rfc" /></pubDate>
  </item>
</xsl:for-each>
</channel>
</rss>
</xsl:template>
</xsl:stylesheet>
